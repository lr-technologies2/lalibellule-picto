from kivy.graphics import Line, Color
from kivy.app import App


class PictoCurrentSelection(App):

    def draw_border(self, idx):
        """Draw the border on the highlited pictogram

        Args:
            idx (int): Index of the pictogram to highlight
        """
        picto = list(reversed(self.gridpicto.children))[idx]

        with picto.canvas.before:
            Color(1, 1, 1, 1)  # set the color to white
            Line(width=2, rectangle=(picto.x, picto.y,
                 picto.width, picto.height))

    def remove_border(self, idx):
        """
        Remove the border at the specified picto index

        Args:
            idx (int): Index of the pictogram at which a border exist.
        """
        picto = list(reversed(self.gridpicto.children))[idx]

        with picto.canvas.before:
            Color(0, 0, 0, 1)  # set the color to black
            Line(width=2, rectangle=(picto.x, picto.y,
                 picto.width, picto.height))
