from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from pictosChoices import PictosChoices


class PictosChoosen(BoxLayout):
    """
    This class regroups the action that can be done on the app screen
    """

    def previous_picto(self):
        """
        Switch to the previous pictogram in the grid
        This function is triggered when the "previous" button is clicked
        """
        self.ids.pictoschoices.select_previous_picto()

    def next_picto(self):
        """
        Switch to the next pictogram in the grid
        This function is triggered when the "next" button is clicked
        """
        self.ids.pictoschoices.select_next_picto()

    def select_picto(self):
        """
        Select the currently highlited picogram and place it in the selected screen
        This function is triggered when the "select" button is clicked
        """

        self.grid_selected_pictos.add_widget(
            self.ids.pictoschoices.get_selected_picto_image())

    def clear_pictos(self):
        """
        Remove all the seelected pictograms
        This function is triggered when the "Clear" button is clicked
        """
        self.grid_selected_pictos.clear_widgets()

    def remove_picto(self):
        """
        Remove the last selected pictogram
        This function is triggered when the "Remove" button is clicked
        """
        nbChildrens = len(self.grid_selected_pictos.children)
        if (nbChildrens > 0):
            lastItemAdded = self.grid_selected_pictos.children[0]
            self.grid_selected_pictos.remove_widget(lastItemAdded)


class PictoApp(App):

    def build(self):
        return PictosChoosen()


if __name__ == "__main__":
    PictoApp().run()
