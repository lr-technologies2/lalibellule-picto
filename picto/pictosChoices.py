import os
from glob import glob
from kivy.properties import StringProperty
from kivy.uix.image import Image
from pictoCurrentSelection import PictoCurrentSelection
from kivy.uix.gridlayout import GridLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.clock import Clock


class PictosChoices(GridLayout):
    """
    This class is used to handle the list of pictograms to be choosed.
    """

    # a augmenter si la bordure s'affiche avant que le widget ne soit render (trait blanc en bas à gauche)
    TIME_BEFORE_CALLBACK = 0.50

    images_loaded = []  # Liste des images chargée au lancement
    current_images_paginated = []  # Liste des pictogrammes de la pagination actuelle
    pagination_size = 12  # taille de la pagination des images
    first_picto_of_page_to_be_selected = True
    current_page = 1
    nb_pages = 0
    selected_image = StringProperty(None)
    current_picto_index = 0
    gridpicto = GridLayout(cols=4, spacing=(10, 10))

    def __init__(self, **kwargs):
        super(PictosChoices, self).__init__(**kwargs)
        self.load_all_pictos_images()
        self.compute_pagination()
        self.toggle_picto_and_pagination()

        Clock.schedule_once(self.force_select_first_picto,
                            self.TIME_BEFORE_CALLBACK)

    def toggle_picto_and_pagination(self):
        self.get_current_pictos_paginated()
        self.add_widget(self.get_paginated_images_on_screen())
        self.add_widget(self.get_label_pagination())

    def load_all_pictos_images(self):
        """Load all images in the <images> folder into pictograms
        """
        curdir = os.path.dirname(__file__)
        for filename in glob(os.path.join(curdir, 'images', '*')):
            picto = Image(source=filename)
            self.images_loaded.append(picto)

    def compute_pagination(self):
        """
        Compute the label of type x/y  with
        x = current self.current_page
        y = nb pages. Ex : 11 images and packages of 5 --> y = 3 (5 + 5 + 1 = 3 pages) 
        """
        nb_pages = len(self.images_loaded) // self.pagination_size
        if (len(self.images_loaded) % self.pagination_size > 0):
            nb_pages += 1

        self.nb_pages = nb_pages

    def get_current_pictos_paginated(self):
        """Add images in Widget to be toggled on the screen
        Get the current_page of a size of pagination
        For example: pagination = 4 and current_page = 1   ====> images_loaded[0 -> 3]
        For example: pagination = 4 and current_page = 3   ====> images_loaded[8 -> 11]
        So it returns images_loaded[
            (current_page - 1) * pagination
            -> 
            ((current_page - 1) * pagination) + pagination
            ]
        """

        start = (self.current_page - 1) * self.pagination_size
        end = ((self.current_page - 1) * self.pagination_size) + \
            self.pagination_size
        self.current_images_paginated = self.images_loaded[start:end]
        self.current_images_paginated = self.current_images_paginated

    def get_paginated_images_on_screen(self):
        """
        Prepare the gridLayout with pictos paginated
        """
        self.gridpicto.clear_widgets()
        for image in self.current_images_paginated:
            self.gridpicto.add_widget(image)

        self.gridpicto.rows = len(self.current_images_paginated)
        self.gridpicto.padding = 10, 10

        return self.gridpicto

    def get_label_pagination(self):
        """
        Prepare the label current_page/max_page
        """
        return Label(text=str(self.current_page) + '/' + str(self.nb_pages),
                     color=[1, 1, 1, 1], size_hint_y=.1)

    def force_select_first_picto(self, dt):
        if self.first_picto_of_page_to_be_selected:
            self.current_picto_index = -1
            self.select_next_picto()
            self.first_picto_of_page_to_be_selected = False

    def force_select_last_picto(self, dt):
        if self.first_picto_of_page_to_be_selected:
            self.current_picto_index = len(self.current_images_paginated)
            self.select_previous_picto()
            self.first_picto_of_page_to_be_selected = False

    def select_next_picto(self):
        """
        Switch to the next pictogram in the grid
        """

        if self.current_picto_index < len(self.current_images_paginated) - 1:
            self.select_next_picto_and_stay_on_page()
        else:
            self.select_next_picto_and_change_page()

    def select_previous_picto(self):
        """
        Switch to the previous pictogram in the grid
        """

        if self.current_picto_index > 0:
            self.select_previous_picto_and_stay_on_page()
        else:
            self.select_previous_picto_and_change_page()

    def select_previous_picto_and_change_page(self):
        """
        handle the changing of page in the pagination
        """
        self.clear_widgets()

        if not self.first_picto_of_page_to_be_selected:
            PictoCurrentSelection.remove_border(
                PictosChoices, self.current_picto_index)

        self.first_picto_of_page_to_be_selected = True
        if self.current_page > 1:
            self.current_page -= 1
        else:
            self.current_page = self.nb_pages

        self.toggle_picto_and_pagination()
        self.current_picto_index = 0
        self.selected_image = self.current_images_paginated[self.current_picto_index].source
        Clock.schedule_once(self.force_select_last_picto,
                            self.TIME_BEFORE_CALLBACK)

    def select_previous_picto_and_stay_on_page(self):
        """
        Select previous picto and stay on the same page
        """
        if not self.first_picto_of_page_to_be_selected:
            PictoCurrentSelection.remove_border(
                PictosChoices, self.current_picto_index)
        self.current_picto_index -= 1
        self.selected_image = self.current_images_paginated[self.current_picto_index].source
        PictoCurrentSelection.draw_border(
            PictosChoices, self.current_picto_index)

    def select_next_picto_and_change_page(self):
        """
        handle the changing of page in the pagination
        """
        self.clear_widgets()

        PictoCurrentSelection.remove_border(
            PictosChoices, self.current_picto_index)
        self.first_picto_of_page_to_be_selected = True
        if self.current_page < self.nb_pages:
            self.current_page += 1
        else:
            self.current_page = 1

        self.toggle_picto_and_pagination()
        self.current_picto_index = len(self.current_images_paginated) - 1
        self.selected_image = self.current_images_paginated[self.current_picto_index].source
        Clock.schedule_once(self.force_select_first_picto,
                            self.TIME_BEFORE_CALLBACK)

    def select_next_picto_and_stay_on_page(self):
        """
        Select next picto and stay on the same page
        """

        PictoCurrentSelection.remove_border(
            PictosChoices, self.current_picto_index)
        self.current_picto_index += 1
        self.selected_image = self.current_images_paginated[self.current_picto_index].source
        PictoCurrentSelection.draw_border(
            PictosChoices, self.current_picto_index)

    def get_selected_picto_image(self):
        """
        Return the currently highlited picogram 
        """
        self.selected_image = self.current_images_paginated[self.current_picto_index].source
        return Image(source=self.selected_image)
