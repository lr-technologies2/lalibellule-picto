# LaLibellule-Picto
A pictogram based communication tool.


## Installation
### *Kivy Installation*
[Getting Started » Installing Kivy](https://kivy.org/doc/stable/gettingstarted/installation.html)

On Windows <br>
- Setup terminal and pip

    $ python -m pip install --upgrade pip setuptools virtualenv

- Create virtual environment

    $ python -m virtualenv kivy_venv

- Windows default CMD

    $ kivy_venv\Scripts\activate

- or bash terminal on Windows

    $ source kivy_venv/Scripts/activate

- Install Kivy

    $ python -m pip install "kivy[full]" kivy_examples    
    

## Usage

    $ cd lalibellule-picto
    $ kivy_venv\Scripts\activate
    $ python picto/pictoChoosen.py

  ![Alt text](interface.png)  

- the right arrow and the left arrow are used to navigate into the picto's list  

- The "select" button is used to select the picto and add it to the "Ma phrase" section

    ![Alt text](<select_button.png>)

- The "clear" button is used to clear all the pictos from the "Ma phrase" section

    <img src="clear_button.png" width="5%" height="5%">

- The "remove" button is used to remove the last added picto from the "Ma phrase" section

    <img src="remove_button.png" width="5%" height="5%">


## Support
The library for GUIs is Kivy : https://kivy.org

Documentation : https://kivy.org/doc/stable/gettingstarted/intro.html

After all this I draw your attention to : https://kivy.org/doc/stable/examples/gen__demo__kivycatalog__main__py.html

